OmnImages
=========

- Utilitaire de traitements divers des fichiers d'images -

Omnimages permet de travailler sur des fichiers d'images, et notamment sur des
photos venant d'APN (Opérations : renommer, classer, redimensionner, dater, etc...)

OmnImages reprend les fonctionalités de MultiScript, script créé par soupaloignon,
mais il a été entièrement réécrit et complété d'autres fonctionnalités.

----

Dépendances :
zenity, jhead, exiv2, imagemagick, libimage-exiftool-perl, rsync

----

Version :
0.1.2 2020/05/02    Modifications des fonctions J1, J3 et T1
0.1.1 2016/07/15	Version "Ouvrir avec..."
0.1.0 2015/04/14	Version "nautilus-scripts"
Testé sous Ubuntu-Mate 14.04 (Trusty)

----

INSTALLATION
------------

- Paquet deb:

https://framagit.org/erresse/Omnimages/raw/master/omnimages_0.1_all.deb

- Dernière version du script:

Dans un terminal, déplacez-vous dans un répertoire de votre path, puis copiez-coller
et lancez cette ligne (dans votre bin/ par exemple) :

`wget -N -t 5 -T 10 https://framagit.org/erresse/Omnimages/raw/master/omnimages && chmod +x omnimages`

----

UTILISATION
-----------

Appel de Omnimages dans le gestionnaire de fichiers:

- Phase d'initialisation : Pour chaque type d'images (jpeg, gif, png...) susceptible d'être inclus,
  sélectionner un fichier d'image et associer Omnimages par clic droit -> "Propriétés", puis onglet
  "Ouvrir avec...", chercher Omnimages et l'ajouter. Cette opération est à faire une seule fois.

- Utilisation courante : Après sélection du(es) fichier(s), faire un clic droit -> "Ouvrir avec..."
  et cliquer sur "Omnimages" dans la liste des applications proposées.

Le script travaillera sur les images sélectionnées contenues dans le répertoire courant
(et uniquement dans ce répertoire, il n'est pas récursif, c'est voulu).

Le script travaille beaucoup avec les données exif des photos JPEG, donc, malgré les
contrôles qu'il effectue, si ces données ne sont pas cohérentes, le résultat final
pourra être incorrect ;)

En fonction du volume de fichiers à traiter et des traitements exécutés, nombre
d'opérations peuvent durer assez longtemps... Soyez patients, ça tourne !

Prenez la précaution de tester vos manips sur des copies de fichiers, avant de les
exécuter sur les données réelles du répertoire, afin d'éviter de les perdre...

Forum
-----

Le fil de discussion sur le forum d'Ubuntu fr : 
https://forum.ubuntu-fr.org/viewtopic.php?pid=21551011#p21551011

Références
----------

L'historique du script MultiScript de soupaloignon :
https://forum.ubuntu-fr.org/viewtopic.php?pid=3371321

Concepteur/Mainteneur
-------

**Roger Sauret** : "erresse" <rogersauret@free.fr><br />

*Licence* : LPRAB http://sam.zoy.org/lprab/ ( ~ Domaine Public )

